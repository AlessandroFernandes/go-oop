package main

import (
	"bank/Account"
	"bank/User"
	"fmt"
)

func main() {
	alessandroCC := Account.ContaCorrente{User: User.User{"Alessandro Fernandes", 32, "São Vicente"}, Ag: 001, Account: 522}
	alessandroCC.Depositar(20000.00)

	fernandaCC := Account.ContaPoupanca{User: User.User{"Fernanda Fernandes", 29, "São Vicente"}, Ag: 001, Account: 522}
	fernandaCC.Depositar(5000.00)

	fmt.Println(alessandroCC)
	Account.PagarBoleto(&alessandroCC, 10000.00)

	fmt.Println(alessandroCC)

}
