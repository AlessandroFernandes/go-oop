package Account

import (
	"bank/User"
	"fmt"
	"strconv"
)

type ContaPoupanca struct {
	User    User.User
	Ag      int
	Account int
	cash    float64
}

func (c *ContaPoupanca) Sacar(price float64) string {
	state := c.cash > 0 && price <= c.cash
	if state {
		c.cash -= price
		return "Successful withdrawal, new cash: " + strconv.FormatFloat(c.cash, 'f', 1, 64)
	} else {
		return "Unrealized withdrawal"
	}
}

func (c *ContaPoupanca) Depositar(price float64) string {
	if price > 0 {
		c.cash += price
		return "Deposit made successfully, new cash: " + strconv.FormatFloat(c.cash, 'f', 1, 64)
	} else {
		return "Deposit not allowed"
	}

}

func (c *ContaPoupanca) Transferencia(price float64, acc *ContaPoupanca) {
	state := c.cash >= price && price > 0
	if state {
		c.cash -= price
		acc.Depositar(price)
		fmt.Println("Transfer complete")
	} else {
		fmt.Println("Transfer not allowed")
	}
}

func (c *ContaPoupanca) GetCash() float64 {
	return c.cash
}
